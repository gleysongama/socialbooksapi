package br.com.gg.socialbooks.repository;

import br.com.gg.socialbooks.domain.Autor;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author spider
 */
public interface AutoresRepository extends JpaRepository<Autor, Long>{
    
}
