package br.com.gg.socialbooks.repository;

import br.com.gg.socialbooks.domain.UserRole;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRolesRepository extends CrudRepository<UserRole, Long> {

    /*Code HQL*/
    @Query("select a.role from UserRole a, User b where b.userName=?1 and a.userName=b.userName")
    public List<String> findRoleByUserName(String username);

}
