package br.com.gg.socialbooks.repository;

import br.com.gg.socialbooks.domain.Comentario;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author spider
 */
public interface ComentariosRepository extends JpaRepository<Comentario, Long>{
    
}
