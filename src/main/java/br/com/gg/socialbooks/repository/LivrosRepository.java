package br.com.gg.socialbooks.repository;

import br.com.gg.socialbooks.domain.Livro;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author spider
 */
public interface LivrosRepository extends JpaRepository<Livro, Long>{
    
}
