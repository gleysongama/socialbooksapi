package br.com.gg.socialbooks.services.exception;

/**
 *
 * @author spider
 */
public class LivroNaoEncontradoException extends RuntimeException {
    
    public LivroNaoEncontradoException(String mensagem) {
        super(mensagem);
    }
    
    public LivroNaoEncontradoException(String mensagem, Throwable causa) {
        super(mensagem, causa);
    }
}
