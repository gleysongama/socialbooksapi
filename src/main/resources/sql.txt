use socialbooks;

INSERT INTO users(username,email,password,enabled)
VALUES ('gleysongama','gleysongama@gmail.com.com','gleysongama', true);
INSERT INTO users(username,email,password,enabled)
VALUES ('user','user@socialbook.com','user', true);
 
INSERT INTO user_roles (username, role)
VALUES ('gleysongama', 'ROLE_ADMIN');
INSERT INTO user_roles (username, role)
VALUES ('user', 'ROLE_USER');
